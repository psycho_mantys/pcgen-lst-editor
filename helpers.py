#!/usr/bin/env python

import gi
gi.require_version('Gtk', '3.0')

def set_column_width(column, width, renderer, pan = True):
	column_width = column.get_width()
#	print "column %s size %s" % (column.get_title(), column_width)
	renderer.props.wrap_width = column_width
	renderer.props.width_chars = 20
#	renderer.props.min_width = 20
#	if pan:
#		renderer.props.wrap_mode = pango.WRAP_WORD
#	else:
#		renderer.props.wrap_mode = Gtk.WRAP_WORD
	renderer.props.wrap_mode = Gtk.WrapMode.WORD


def create_treeview(row_format, column_titles, row_data, **kwargs):
	# get default args
	filter_func=kwargs.get("filter_func", None)
	edit_callback=kwargs.get("edit_callback", None)
	edit_data_callback=kwargs.get("edit_data_callback", None)

	#Creating the ListStore model
	liststore = Gtk.ListStore(*row_format)
	for row in row_data:
		liststore.append(row[:len(row_format)])

	if filter_func:
		#Creating the filter, feeding it with the liststore model
		filter = liststore.filter_new()
		#setting the filter function, note that we're not using the
		filter.set_visible_func(filter_func)
		#creating the treeview, making it use the filter as a model, and adding the columns
		treeview = Gtk.TreeView.new_with_model(filter)
	else:
		treeview=Gtk.TreeView(model=liststore)

	for i, column_title in enumerate(column_titles):
		renderer=Gtk.CellRendererText()
		if edit_callback:
			renderer.connect('edited', edit_callback, int(i), edit_data_callback)
			renderer.set_property('editable', True)
		column = Gtk.TreeViewColumn(column_title, renderer, text=i)
		column.set_sizing(True)
		column.set_min_width(20)
		column.set_sizing(Gtk.TreeViewColumnSizing.AUTOSIZE)
		column.connect_after("notify::width", set_column_width, renderer)
		treeview.append_column(column)

	return treeview, liststore


class Handler_finder(object):
	"""Searches for handler implementations across multiple objects.
	"""
	# See <http://stackoverflow.com/questions/4637792> for why this is
	# necessary.
	def __init__(self, backing_objects):
		self.backing_objects = backing_objects

	def __getattr__(self, name):
		for o in self.backing_objects:
			if hasattr(o, name):
				return getattr(o, name)
		else:
			raise AttributeError("%r not found on any of %r" %(name, self.backing_objects))


