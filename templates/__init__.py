#!/usr/bin/env python


TEMPLATE=dict()

from .add_feat import Wizard
TEMPLATE["add_feat"]=Wizard

from .add_spell import Wizard
TEMPLATE["add_spell"]=Wizard

from .add_trait import Wizard
TEMPLATE["add_trait"]=Wizard

