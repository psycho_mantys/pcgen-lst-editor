#!/usr/bin/env python

import sys
import templates.skeleton as sk

from windows.popup_error import Popup_error


class Wizard(sk.Skeleton):
	def __init__(self, editor):
		super().__init__(editor, "templates/add_trait.glade")

	def on_ok(self, window):
		try:
			trait_name=self.builder.get_object("trait_name").get_text()
			trait_type=self.builder.get_object("trait_type").get_text()

			if trait_name=="":
				Popup_error("Blank trait name!!", self)
				self.on_destroy()
				return False
			
			if trait_type=="":
				Popup_error("No trait type!!", self)
				self.on_destroy()
				return False

			add_entry=[trait_name]

			sk.add_to_entry("DESC", add_entry, self.builder)
			sk.add_to_entry("CATEGORY", add_entry, self.builder)
			sk.add_to_entry("SOURCEPAGE", add_entry, self.builder)

			add_entry.append("KEY:Trait ~ "+trait_name)

			add_entry.append("TYPE:Trait.BasicTrait."+trait_type+"Trait")

			req="PREMULT:1,[PREABILITY:1,CATEGORY=Special Ability,Trait ~ "+trait_name+"],[PREVAREQ:BypassTraitRestriction,1],[!PREABILITY:1,CATEGORY=Special Ability,TYPE."+trait_type+"Trait]"
			add_entry.append(req)

			print(add_entry)
			self.editor.push_lst_entry(add_entry)

			self.on_destroy()
		except:
			print("Dev error:", sys.exc_info()[0])
			raise

