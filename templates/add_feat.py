#!/usr/bin/env python

import sys
import templates.skeleton as sk

from windows.popup_error import Popup_error


class Wizard(sk.Skeleton):
	def __init__(self, editor):
		super().__init__(editor, "templates/add_feat.glade")

	def on_ok(self, window):
		try:
			feat_name=self.builder.get_object("feat_name").get_text()

			if feat_name=="":
				Popup_error("Blank feat name!!", self)
				self.on_destroy()
				return False
			
			add_entry=[feat_name]

			sk.add_to_entry("DESC", add_entry, self.builder)
			sk.add_to_entry("BENEFIT", add_entry, self.builder)
			sk.add_to_entry("CATEGORY", add_entry, self.builder)
			sk.add_to_entry("MULT", add_entry, self.builder)

			print(add_entry)
			self.editor.push_lst_entry(add_entry)

			self.on_destroy()
		except:
			print("Dev error:", sys.exc_info()[0])
			raise

