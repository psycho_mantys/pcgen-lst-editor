#!/usr/bin/env python

import sys
import templates.skeleton as sk

from windows.popup_error import Popup_error


class Wizard(sk.Skeleton):
	def __init__(self, editor):
		super().__init__(editor, "templates/add_spell.glade")

	def on_ok(self, window):
		try:
			spell_name=self.builder.get_object("spell_name").get_text()

			if spell_name=="":
				Popup_error("Blank spell name!!", self)
				self.on_destroy()
				return False
			
			add_entry=[spell_name]

			sk.add_to_entry("SCHOOL", add_entry, self.builder)
			sk.add_to_entry("SUBSCHOOL", add_entry, self.builder)
			sk.add_to_entry("DESCRIPTOR", add_entry, self.builder)
			sk.add_to_entry("CLASSES", add_entry, self.builder)
			sk.add_to_entry("DOMAINS", add_entry, self.builder)
			sk.add_to_entry("TYPE", add_entry, self.builder)
			sk.add_to_entry("COMPS", add_entry, self.builder)
			sk.add_to_entry("CASTTIME", add_entry, self.builder)
			sk.add_to_entry("RANGE", add_entry, self.builder)
			sk.add_to_entry("TARGETAREA", add_entry, self.builder)
			sk.add_to_entry("DURATION", add_entry, self.builder)
			sk.add_to_entry("SAVEINFO", add_entry, self.builder)
			sk.add_to_entry("SPELLRES", add_entry, self.builder)
			if not sk.add_to_entry("DESC", add_entry, self.builder):
				Popup_error("Missing DESC, required!", self)
				self.on_destroy()
				return False
			sk.add_to_entry("COST", add_entry, self.builder)
			sk.add_to_entry("XPCOST", add_entry, self.builder)
			sk.add_to_entry("VARIANTS", add_entry, self.builder)

			print(add_entry)
			self.editor.push_lst_entry(add_entry)

			self.on_destroy()
		except:
			print("Dev error:", sys.exc_info()[0])
			raise

