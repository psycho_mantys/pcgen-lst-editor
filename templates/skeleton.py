#!/usr/bin/env python

import gi
gi.require_version('Gtk', '3.0')

import gtk as Gtk
from gi.repository import Gtk

from windows.popup_error import Popup_error


def add_to_entry(entry_name, entry_list, builder):
	field=builder.get_object(entry_name).get_text()
	if field!="":
		entry_list.append(entry_name+":"+field)
		return True
	return False


class Skeleton(object):

	def __init__(self, editor, ui_xml):
		self.ROOT_WINDOW='root_window'			# root window name
		self.UI_XML=ui_xml						# GUI descriptor

		self.app=editor.app
		self.parent_window=editor.root_window
		self.editor=editor

		self.builder = Gtk.Builder()
		self.builder.add_from_file(self.UI_XML)
		self.builder.connect_signals(self)
		self.root_window = self.builder.get_object(self.ROOT_WINDOW)

		self.root_window.set_application(self.app)
		self.root_window.set_transient_for(self.parent_window)

#		self.root_window.maximize()
		self.root_window.show_all()

	def on_ok(self, window):
		pass

	def on_cancel(self, *kargs):
		self.on_destroy()
	def on_destroy(self, *kargs):
		self.root_window.destroy()

