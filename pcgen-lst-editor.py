#!/usr/bin/env python

import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk

import templates

from windows.about import About
from windows.popup_error import Popup_error
from test import Dummy_fc
from helpers import *

import sys
import os
import re

def split_entry(value):
	key=value.split(':',1)

	if len(key)>1:
		return key
	else:
		return ["NO_KEY",key[0]]

def join_entry(value):
	if value[0]=="NO_KEY":
		return value[1]
	else:
		return value[0]+":"+value[1]



def parse_file(file):
	ret=[]
	for line in file:
		if line[0]!=" " and line[0]!="\t" and line[0]!="#" and line[0]!="\n" and line!="\r":
#		if line[0]!="\t" and line[0]!="\n":
			ret.append([w for w in re.split('\t', line) if w!="\n" and w!=" " and w!="\t" and w!="\r" and w])
			# Remove end of line from last entry
			if ret[-1][-1][-1]=='\n' or ret[-1][-1][-1]=='\n\r' or ret[-1][-1][-1]=='\r':
				ret[-1][-1]=ret[-1][-1][:-1]
	return ret

def create_tab_header_box(tab_name):
	# Tab name and close button
	header = Gtk.Box(orientation=Gtk.Orientation.HORIZONTAL)

	title_label = Gtk.Label(label=tab_name)
	close_button = Gtk.Button(image=Gtk.Image(stock=Gtk.STOCK_CLOSE))

	header.pack_start(title_label, expand=True, fill=True, padding=0)
	header.pack_end(close_button, expand=False, fill=False, padding=0)
	header.show_all()
	return close_button, header


class MainWindow():

	ROOT_WINDOW = 'root_window'				# root window name
	UI_XML = 'pcgen-lst-editor.glade'		# GUI descriptor

	def __init__(self, app):
		self.app=app
		self.builder = Gtk.Builder()
		self.builder.add_from_file(self.UI_XML)
		self.builder.connect_signals(self)
		self.root_window = self.builder.get_object(self.ROOT_WINDOW)
		self.root_window.set_application(app)

		## Current selection
		self.liststore_keys=None

		## TAB
		self.filetab = self.builder.get_object('filetab')
		self.filetab.remove_page(-1)


		self.root_window.maximize()
		self.root_window.show_all()

		self.file_open=dict()

	def get_tab_by_filename(self, filename):
		for i in range(self.filetab.get_n_pages()):
			tab=self.filetab.get_nth_page(i)
			head=self.filetab.get_tab_label(tab)
			if head.filename==filename:
				return tab
		return None


	def current_tab_filename(self):
		current_tab=self.get_current_tab()
		if current_tab:
			return self.filetab.get_tab_label(current_tab).filename
		return None

	def current_treeview_key(self):
		current_tab=self.get_current_tab()
		if current_tab:
			return self.filetab.get_tab_label(current_tab).treeview_key
		return None

	def current_index_key(self):
		# get the model and the iterator that points at the data in the model
		(model, iter) = self.current_treeview_key().get_selection().get_selected()
		#key_name=model[iter][0]
		key_index=model.get_path(iter).get_indices()[0]
		return key_index

	def current_treeview_entry(self):
		current_tab=self.get_current_tab()
		if current_tab:
			return self.filetab.get_tab_label(current_tab).treeview_entry
		return None

	def current_liststore_entry(self):
		current_tab=self.get_current_tab()
		if current_tab:
			return self.filetab.get_tab_label(current_tab).liststore_entry
		return None

	def current_liststore_key(self):
		current_tab=self.get_current_tab()
		if current_tab:
			return self.filetab.get_tab_label(current_tab).liststore_key
		return None

	def current_index_entry(self):
		# get the model and the iterator that points at the data in the model
		(model, iter) = self.current_treeview_entry().get_selection().get_selected()
		#key_name=model[iter][0]
		entry_index=model.get_path(iter).get_indices()[0]
		return entry_index

	def save_all_files(self, button):
		for file in self.file_open.keys():
			with open(file,'w') as f:
				for entry in self.file_open[file]["entrys"]:
					f.write('\t\t\t'.join(entry)+"\n")

	def on_file_choose(self, fc):
		
		filename=os.path.abspath(fc.get_filename())

		print("Escolhido "+filename)

		if (filename in self.file_open):
			Popup_error("File alredy open!!", self)
			return False

		file=open(filename, "r")

		self.file_open[filename]=dict()
		self.file_open[filename]["file"]=file.readlines()
		file.seek(0)
		self.file_open[filename]["entrys"]=parse_file(file)

		tb=Gtk.Builder()
		tb.add_objects_from_file(self.UI_XML, ("tab_header", "tab_body", "keys", "entrys", "close"))
		tb.connect_signals(self)

		tab_header=tb.get_object("tab_header")

		tab_header.filename=filename

		tab_header.treeview_key=tb.get_object("key_entrys")
		tab_header.liststore_key=tb.get_object("keys")
		tab_header.liststore_key.clear()

		tab_header.treeview_entry=tb.get_object("entry_body")
		tab_header.liststore_entry=tb.get_object("entrys")
		tab_header.liststore_entry.clear()


		tab_body=tb.get_object("tab_body")

		tab_filename=tb.get_object("tab_filename")
		tab_filename.set_text(os.path.basename(filename))


		for row in self.file_open[filename]["entrys"]:
			print(row[:len( (str,) )])
			tab_header.liststore_key.append(row[:len( (str,) )])

		self.filetab.insert_page(tab_body, tab_header, -1)

		self.root_window.show_all()
		tab_body.show_all()
		tab_header.treeview_key.show_all()
		tab_header.treeview_entry.show_all()

	def on_remove_key(self, button):
		treeview=self.current_treeview_key()

		# get the model and the iterator that points at the data in the model
		(model, iter) = treeview.get_selection().get_selected()
		field_index=model.get_path(iter).get_indices()[0]

		model.remove(iter)
		del self.file_open[filename]["entrys"][field_index]

	def on_add_key(self, button):
		placeholder_entry=["NO_VALUE"]
		liststore=self.current_liststore_key()
		filename=self.current_tab_filename()

		liststore.append(placeholder_entry)
		self.file_open[filename]["entrys"].append(placeholder_entry)

	def on_add_entry(self, *kargs):
		liststore=self.current_liststore_entry()
		placeholder_entry=["NO_KEY","VALUE"]
		filename=self.current_tab_filename()
		key_index=self.current_index_key()

		liststore.append(placeholder_entry)
		self.file_open[filename]["entrys"][key_index].append(":".join(placeholder_entry))

	def on_remove_entry(self, *kargs):
		treeview=self.current_treeview_entry()
		entry_index=self.current_index_entry()
		key_index=self.current_index_key()
		filename=self.current_tab_filename()

		# get the model and the iterator that points at the data in the model
		(model, iter) = treeview.get_selection().get_selected()

		model.remove(iter)
		del self.file_open[filename]["entrys"][key_index][entry_index]
	
	def on_entry_c0_edited(self, cell_renderer_text, column, new_text):
		return self.on_entry_edited(cell_renderer_text, column, new_text, 0)

	def on_entry_c1_edited(self, cell_renderer_text, column, new_text):
		return self.on_entry_edited(cell_renderer_text, column, new_text, 1)

	def on_entry_edited(self, cell_renderer_text, column, new_text, row):
		column=int(column)
		filename=self.current_tab_filename()
		liststore=self.current_liststore_entry()
		key_index=self.current_index_key()
		entry_index=self.current_index_entry()

		key, value=split_entry(self.file_open[filename]["entrys"][key_index][entry_index])

		if entry_index!=0:
			if row==0:
				self.file_open[filename]["entrys"][key_index][entry_index]=join_entry([new_text,value])
			else:
				self.file_open[filename]["entrys"][key_index][entry_index]=join_entry([key,new_text])
			liststore[entry_index][row]=new_text


	def on_key_edited(self, cell_renderer_text, column, new_text):
		filename=self.current_tab_filename()
		liststore_key=self.current_liststore_key()
		liststore_entry=self.current_liststore_entry()
		column=int(column)
		key_index=self.current_index_key()
		#entry_index=self.current_index_entry()

		self.file_open[filename]["entrys"][key_index][0]=new_text
		liststore_key[key_index][0]=new_text

		if liststore_entry:
			key, value=split_entry(new_text)
			liststore_entry[0][0]=key
			liststore_entry[0][1]=value



	def on_treeview_selection_changed(self, selection):
		# get the model and the iterator that points at the data in the model
		(model, iter) = selection.get_selected()
		entry_name=model[iter][0]
		entry_index=model.get_path(iter).get_indices()[0]
		key_index=self.current_index_key()

		filename=self.current_tab_filename()

		i=self.file_open[filename]["entrys"][key_index]

		liststore_entry=self.current_liststore_entry()

		# remove old entrys
		liststore_entry.clear()

		# Add body of selected  key
		for value in i:
			#print(split_entry(value))
			liststore_entry.append(split_entry(value))



	def on_destroy(self,window):
		"Terminate program at window destroy"
		self.app.quit()
		#self.root_window.destroy()
		#Gtk.main_quit()

	def push_lst_entry(self, entry):
		filename=self.current_tab_filename()
		liststore_keys=self.current_liststore_key()
		if filename and liststore_keys:
			#print(entry)
			self.file_open[filename]["entrys"].append(entry)
			liststore_keys.append([entry[0]])

	def get_current_tab(self):
		return self.filetab.get_nth_page(self.filetab.get_current_page())

	def on_tab_close(self, button):
		filename=button.get_parent().filename

		del self.file_open[filename]
		page_num=self.filetab.page_num(self.get_tab_by_filename(filename))
		self.filetab.remove_page(page_num)

	def on_template_add_feat(self,dummy):
		templates.TEMPLATE["add_feat"](self)
	def on_template_add_spell(self,dummy):
		templates.TEMPLATE["add_spell"](self)
	def on_template_add_trait(self,dummy):
		templates.TEMPLATE["add_trait"](self)
	def on_about(self, dump):
		About(self)


class MyApplication(Gtk.Application):
	main_window=None
	def __init__(self):
		Gtk.Application.__init__(self)

	def do_activate(self):
		self.main_window = MainWindow(self)
		# Remove later
		self.main_window.on_file_choose(Dummy_fc('test.lst'))
		#self.main_window.on_file_choose(Dummy_fc('test.lst'))

	def do_startup(self):
		Gtk.Application.do_startup(self)


if __name__ == '__main__':
	app = MyApplication()
	exit_status=app.run(sys.argv)
	sys.exit(exit_status)

