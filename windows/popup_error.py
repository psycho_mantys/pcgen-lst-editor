#!/usr/bin/env python

import gi
gi.require_version('Gtk', '3.0')

import gtk as Gtk
from gi.repository import Gtk


class Popup_error():
	def __init__(self, error_msg, editor):

		self.app=editor.app
		self.parent_window=editor.root_window
		self.editor=editor

		message = Gtk.MessageDialog(self.parent_window,
			Gtk.DialogFlags.MODAL, 
			Gtk.MessageType.ERROR, 
			Gtk.ButtonsType.OK,
			error_msg)

#		message.set_markup(error_msg)
		message.run()
		message.destroy()

