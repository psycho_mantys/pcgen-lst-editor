#!/usr/bin/env python

import gi
gi.require_version('Gtk', '3.0')
from gi.repository import GObject

def test_signals(obj):
    """Helper function to test signals

     It connects all `obj` signals to a dummy function
    which simply prints out all given arguments.

    Args:
      obj: A ``gobject.GObject``

    """
    if not isinstance(obj, GObject.GObject):
        print("test_signals failed: obj must be a GObject")
        return
    def foo(*args):
        print("Signal %r emitted by %r:" % (args[-2],
                            args[-1].__class__.__name__), args[:-2])
    for signame in GObject.signal_list_names(obj):
        obj.connect(signame, foo, signame, obj)


class Dummy_fc():
	def __init__(self, filename):
		self.filename=filename

	def get_filename(self):
		return self.filename

