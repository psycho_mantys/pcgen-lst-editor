PCGen LST Editor
=================================================

A Editor to LST files for PCGen

On development!

Screenshot
=================================================

![Interface example](https://bitbucket.org/psycho_mantys/pcgen-lst-editor/raw/master/media/img/screeshot_01.png)

![Another example](https://bitbucket.org/psycho_mantys/pcgen-lst-editor/raw/master/media/img/screeshot_02.png)

See More
=================================================

https://bitbucket.org/psycho_mantys/pcgen-lst-editor/wiki/
